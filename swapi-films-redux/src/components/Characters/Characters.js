import React, { useEffect } from 'react';
import Loader from '../Loader/Loader';
import axios from 'axios';
import { connect } from 'react-redux';

const Characters = (props) => {
  const { film, setCharacters, setIsLoading, isLoading, characters } = props;

  useEffect(() => {
    if (!film || !film.length) {
      axios.all(film.characters.map((c) => axios(c))).then((res) => {
        setCharacters(res.map((i) => i.data.name));
        setIsLoading(false);
      });
    }
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  const charactersName = characters.map((c) => c).join(', ');
  return <div>{charactersName}</div>;
};
const mapStateToProps = (state) => {
  return {
    characters: state.characters.data,
    isLoading: state.characters.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCharacters: (data) =>
      dispatch({ type: 'SET_CHARACTERS', payload: data }),
    setIsLoading: (isLoading) =>
      dispatch({ type: 'SET_CHARACTER_LOADING', payload: isLoading }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Characters);
