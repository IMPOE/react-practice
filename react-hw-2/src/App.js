import React, { Component } from 'react';
import Loader from './components/Loader/Loader.js';
import Watch from './components/Watch/Watch.js';
import './App.scss';

class App extends Component {
  state = {
    items: [],
    isLoading: true,
  };

  componentDidMount() {
    fetch('item.json')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ items: data, isLoading: false });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { isLoading, items } = this.state;

    if (isLoading) {
      return <Loader />;
    }

    const watchItems = items.map((e) => <Watch key={e.article} item={e} />);

    return (
      <div id="App" className="App">
        <div className="wrapper">{watchItems}</div>
      </div>
    );
  }
}

export default App;
