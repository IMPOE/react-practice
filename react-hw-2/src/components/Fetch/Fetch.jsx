import React, { Component } from 'react';

export default class Fetch extends Component {
  render() {
    const { items } = this.props;
    return (
      <>
        {fetch('item.json')
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            // this.setState({ items: data, isLoading: false });
            items.push(data);
            console.log(items);
          })
          .catch((error) => {
            console.log(error);
          })}
      </>
    );
  }
}
