import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';
class Modal extends PureComponent {
  render() {
    const {
      header,
      closeButton,
      text,
      backgroundColor,
      onClick,
      closeModal,
      id,
    } = this.props;
    return (
      <div>
        <div className="modal" style={{ backgroundColor: backgroundColor }}>
          <div className="header__modal">
            {header}
            {closeButton && <button className="close__btn">X</button>}
          </div>

          <div className="body__modal">{text}</div>

          <div className="footer__modal">
            <button className="footer__ok" id={id} onClick={onClick}>
              OK
            </button>
            <button className="footer__cancel" onClick={closeModal}>
              Cancel
            </button>
          </div>
        </div>
      </div>
    );
  }
}
Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  closeModal: PropTypes.func,
  id: PropTypes.number,
};
Modal.defaultProps = {
  header: 'head',
  text: 'write your text',
  backgroundColor: 'gray',
};
export default Modal;
