import React, { PureComponent } from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
  render() {
    const { text, className, modal, onClick, renderModal } = this.props;
    return (
      <div>
        <button onClick={onClick} className={className}>
          {text}
        </button>
        <div>{renderModal && modal}</div>
      </div>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  modal: PropTypes.element,
  onClick: PropTypes.func,
  renderModal: PropTypes.bool,
};
Button.defaultProps = {
  text: 'text',
  className: 'card__btn',
  renderModal: false,
};
export default Button;
