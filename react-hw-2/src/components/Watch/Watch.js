import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './Watch.scss';
import Logo from './IMG/love.svg';
import Heart from './IMG/heart.svg';
import Button from '../Button/Button.js';
import Modal from '../Modal/Modal.js';

class Watch extends PureComponent {
  state = {
    favorite: false,
    renderModal: false,
    cart: [],
  };

  renderModal = (e) => {
    this.setState({ renderModal: !this.state.renderModal });
    if (e.target.id !== '') {
      this.setState((newState) => {
        localStorage.setItem(`card-${e.target.id}`, e.target.id);
      });
    }
  };

  closeModal = () => {
    this.setState({ renderModal: false });
  };

  addFavorite = (e) => {
    this.setState({ favorite: !this.state.favorite });
    localStorage.getItem(`favorite-${e.target.id}`) !== null
      ? localStorage.removeItem(`favorite-${e.target.id}`)
      : this.setState((newState) => {
          localStorage.setItem(`favorite-${e.target.id}`, e.target.id);
        });
  };

  render() {
    const { item } = this.props;

    const { favorite, renderModal } = this.state;

    return (
      <div className="card__container">
        <div className="card">
          <div className="card__header">
            <div onClick={this.addFavorite}>
              {favorite ? (
                <img
                  className="logo__img"
                  id={item.article}
                  src={Heart}
                  alt=""
                />
              ) : (
                <img
                  id={item.article}
                  className="logo__img"
                  src={Logo}
                  alt=""
                />
              )}
            </div>

            <span className="card__name">{item.name}</span>
          </div>
          <div>
            <img className="card__img" src={item.src} alt="" />
          </div>
          <div className="card__price">Цена: {item.price}</div>
          <div className="card__footer">
            <div className="card__color-block">
              <div className="card__color">
                {item.color}
                <hr
                  className="card__color-style"
                  style={{ backgroundColor: item.color }}
                ></hr>
              </div>
            </div>
            <div className="card__article">Артикль: {item.article}</div>
          </div>
          <Button
            onClick={this.renderModal}
            renderModal={renderModal}
            modal={
              <Modal
                id={item.article}
                onClick={this.renderModal}
                closeModal={this.closeModal}
                header={'Add to cart'}
                text={'Do you want add this item to cart?'}
              />
            }
            text={'Add to cart'}
            className={'card__btn'}
          />
        </div>
      </div>
    );
  }
}
Watch.propTypes = {
  favorite: PropTypes.bool,
  renderModal: PropTypes.bool,
  cart: PropTypes.array,
  item: PropTypes.object,
};
export default Watch;
