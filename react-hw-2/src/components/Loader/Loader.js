import React, {PureComponent} from 'react';
import "./Loader.scss"

class Loader extends PureComponent {
    render() {
        return (
            <div id="loadingProgressG">
                <div id="loadingProgressG_1" className="loadingProgressG"></div>
            </div>
        );
    }
}

export default Loader;