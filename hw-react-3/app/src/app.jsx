import { React, useEffect, useState } from 'react';
import Header from './content/header/header.jsx';
import Routes from './routes/Routes.jsx';
export default function App() {
  const [storage, setStorage] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const localCart = JSON.parse(localStorage.getItem('cart'));
    const localFavorite = JSON.parse(localStorage.getItem('favorites'));
    if (localCart) {
      setStorage(localCart);
    }
    if (localFavorite) {
      setFavorites(localFavorite);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(storage));
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [storage, favorites]);

  return (
    <div>
      <Header storage={storage} favorites={favorites} />
      <Routes
        setStorage={setStorage}
        storage={storage}
        favorites={favorites}
        setFavorites={setFavorites}
      />
    </div>
  );
}
