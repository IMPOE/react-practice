import React from 'react';
import PropTypes from 'prop-types';
export default function Button({ title, onClick, id, className }) {
  return (
    <>
      <button className={className} id={id} onClick={onClick}>
        {title}
      </button>
    </>
  );
}
Button.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.number,
  className: PropTypes.string,
};
