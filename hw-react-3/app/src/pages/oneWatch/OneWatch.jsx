import { React, useEffect, useState } from 'react';
import axios from 'axios';
import './oneWatch.scss';
export default function OneWatch({ history, location, match }) {
  const [res, setRes] = useState({});
  const [isLoading, setIsloading] = useState(true);
  const itemId = match.params.itemId;

  useEffect(() => {
    setIsloading(true);
    axios(`/api/watch/${itemId}`).then((res) => {
      setRes(res.data.content);
    });
  }, []);

  console.log(res);
  // if (isLoading){
  //   return <Loader/>
  // }

  return (
    <>
      <div className="oneWatch">
        <div className="card">
          <img className="img" src={res.src} alt={res.name}></img>
        </div>
        <div className="name">
          <p>{`Марка: ${res.name}`}</p>
          <p>{`Цена: ${res.price}`}</p>
          <p>{`Цвет: ${res.color}`}</p>
        </div>
        <div></div>
      </div>
      <h2 className="more">Дополнительно :</h2>
      <ul className>
        <li>{`Артикль: ${res.article}`}</li>
        <li>{`${res.name}  - ${res.description}`}</li>
      </ul>
    </>
  );
}
