import { useOnePost } from '../../components/requests/requests';
import Watch from '../../content/watch/Watch';
import './items.scss';
import PropTypes from 'prop-types';
export default function Item({ setStorage, storage, favorites, setFavorites }) {
  const [items] = useOnePost();

  const watch = items.map(({ id, content }) => {
    return (
      <Watch
        key={id}
        id={id}
        name={content.name}
        src={content.src}
        price={content.price}
        color={content.color}
        article={content.article}
        option={true}
        setStorage={setStorage}
        storage={storage}
        favorites={favorites}
        setFavorites={setFavorites}
      />
    );
  });
  return (
    <div>
      <div className="watch__wrapper">{watch}</div>
      <div>
        <span>{`Вы добавили товар с ID : `}</span>
      </div>
    </div>
  );
}

Item.propTypes = {
  setStorage: PropTypes.func,
  storage: PropTypes.array,
  favorites: PropTypes.array,
  setFavorites: PropTypes.func,
  items: PropTypes.array,
};

Item.defaultProps = {
  items: [],
  storage: [],
  favorites: [],
};
