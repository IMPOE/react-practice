import { useOnePost } from '../../components/requests/requests';
import CartWatch from '../../content/CartWatch/CartWatch';
import './cart.scss';
import PropTypes from 'prop-types';
export default function Cart({ setStorage, storage }) {
  const [watch] = useOnePost([]);

  const cartWatch = watch.map(({ id, content }) => {
    function deletFromCart() {
      const local = storage.filter((e) => e !== id);
      setStorage(local);
    }
    for (let key of storage) {
      if (key === id) {
        return (
          <CartWatch
            key={id}
            id={id}
            name={content.name}
            src={content.src}
            price={content.price}
            color={content.color}
            article={content.article}
            deletFromCart={deletFromCart}
          />
        );
      }
    }
  });

  return (
    <div className="cart__container">
      <div>{cartWatch}</div>
      <div></div>
    </div>
  );
}

Cart.propTypes = {
  storage: PropTypes.array,
  setStorage: PropTypes.func,
  watch: PropTypes.array,
};

Cart.defaultProps = {
  storage: [],
  watch: [],
};
