import { useOnePost } from '../../components/requests/requests';
import FavoriteWatch from '../../content/FavoriteWatch/FavoriteWatch';
import PropTypes from 'prop-types';
export default function Favorites({ favorites, setFavorites }) {
  const [items] = useOnePost();

  const cartWatch = items.map(({ id, content }) => {
    function deletFromFavorite() {
      const local = favorites.filter((e) => e !== id);
      setFavorites(local);
    }

    for (let key of favorites) {
      if (key === id) {
        return (
          <FavoriteWatch
            key={id}
            id={id}
            name={content.name}
            src={content.src}
            price={content.price}
            color={content.color}
            article={content.article}
            deletFromFavorite={deletFromFavorite}
            delet={true}
          />
        );
      }
    }
  });
  return <div className="cart__container">{cartWatch}</div>;
}

Favorites.propTypes = {
  favorites: PropTypes.array,
  setFavorites: PropTypes.func,
  items: PropTypes.array,
};

Favorites.defaultProps = {
  items: [],
  favorites: [],
};
