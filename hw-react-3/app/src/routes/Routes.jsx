import { React } from 'react';
import { Switch, Route } from 'react-router-dom';
import Index from '../pages/index/Index';
import Favorites from '../pages/favorites/Favorites';
import Cart from '../pages/cart/Cart';
import Page404 from '../pages/page404/Page404';
import Items from '../pages/items/Items';
import PropTypes from 'prop-types';
import OneWatch from '../pages/oneWatch/OneWatch';
export default function Routes({
  setStorage,
  storage,
  favorites,
  setFavorites,
}) {
  return (
    <Switch>
      <Route exact path="/">
        <Index />
      </Route>
      <Route exact path="/favorites">
        <Favorites favorites={favorites} setFavorites={setFavorites} />
      </Route>
      <Route exact path="/cart">
        <Cart setStorage={setStorage} storage={storage} />
      </Route>
      <Route exact path="/items">
        <Items
          setStorage={setStorage}
          storage={storage}
          favorites={favorites}
          setFavorites={setFavorites}
        />
      </Route>

      <Route exact path="/items/:itemId" component={OneWatch}></Route>

      <Route path="*">
        <Page404 />
      </Route>
    </Switch>
  );
}
Routes.propTypes = {
  favorites: PropTypes.array,
  setFavorites: PropTypes.func,
  storage: PropTypes.array,
  setStorage: PropTypes.func,
};

Routes.defaultProps = {
  storage: [],
  favorites: [],
};
