import React from 'react';
import Button from '../../commons/button/Button';
import PropTypes from 'prop-types';
import './FavoriteWatch.scss';
export default function FavoriteWatch({
  id,
  name,
  src,
  price,
  color,
  article,
  deletFromFavorite,
}) {
  return (
    <div id={id} className="favorite">
      <div className="favorite__name">{name}</div>
      <img className="favorite__img" src={src} alt={name}></img>
      <div className="favorite__price">{`Цена : ${price}`}</div>
      <div className="favorite__color">{color}</div>
      <div className="favorite__article">{`Артикль : ${article}`}</div>
      <Button id={id} title={'Удалить'} onClick={deletFromFavorite} />
    </div>
  );
}

FavoriteWatch.propTypes = {
  id: PropTypes.number,
  src: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  article: PropTypes.number,
  deletFromFavorite: PropTypes.func,
};

FavoriteWatch.defaultProps = {
  id: null,
  src: null,
  price: null,
  color: null,
  article: null,
};
