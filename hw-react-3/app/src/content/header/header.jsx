import { React, useEffect, useState } from 'react';
import { NavLink, Link } from 'react-router-dom';
import './header.scss';
import PropTypes from 'prop-types';
export default function Header({ storage, favorites }) {
  const [cartSum, setCart] = useState([]);
  const [favorite, setFavorites] = useState([]);
  useEffect(() => {
    const localCart = [...new Set(storage)];
    const localFavorites = [...new Set(favorites)];
    setCart(localCart);
    setFavorites(localFavorites);
  }, [storage, favorites]);
  return (
    <nav className="navbar">
      <div className="logo__nav">
        <img className="logo__nav-img" alt="logo"></img>
        <span className="logo__nav-span">WATCH</span>
      </div>
      <ul className="list__menu">
        <li>
          <NavLink className="link" activeClassName="link--active" exact to="/">
            Главная
          </NavLink>
        </li>
        <li>
          <NavLink className="link" activeClassName="link--active" to="/items">
            Часы
          </NavLink>
        </li>
        <li>
          <NavLink className="link" activeClassName="link--active" to="/brands">
            Бренды
          </NavLink>
        </li>
        <li>
          <NavLink
            className="link"
            activeClassName="link--active"
            to="/search?q=react"
          >
            Поиск
          </NavLink>
        </li>
      </ul>
      <div className="nav__options">
        <Link to="/cart">
          <img className="nav__cart" alt="cart"></img>
          <div className="nav__cart-number">{Number(cartSum.length)}</div>
        </Link>
        <Link to="/favorites">
          <img className="nav__favorite" alt="favorite"></img>
          <div className="nav__favotite-number">{Number(favorite.length)}</div>
        </Link>
      </div>
    </nav>
  );
}
Header.propTypes = {
  storage: PropTypes.array,
  favorites: PropTypes.array,
  cartSum: PropTypes.array,
  favorite: PropTypes.array,
};

Header.defaultProps = {
  storage: [],
  favorites: [],
  cartSum: [],
  favorite: [],
};
