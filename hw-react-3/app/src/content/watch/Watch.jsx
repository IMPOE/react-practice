import { React } from 'react';
import './watch.scss';
import Button from '../../commons/button/Button';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
export default function Watch({
  id,
  name,
  src,
  price,
  color,
  article,
  option,
  setStorage,
  storage,
  favorites,
  setFavorites,
  deletFromCart,
  deletFromFavorite,
  delet,
}) {
  function addCart() {
    setStorage([...storage, id]);
  }

  function addFavorite() {
    setFavorites([...favorites, id]);
  }

  return (
    <div id={id} className="card">
      <button className="card__favorite" onClick={addFavorite}></button>
      <div className="card__name">{name}</div>
      <Link exact to={`/items/${id}`} className="link">
        <img className="card__img" src={src} alt={name}></img>
      </Link>
      <div className="card__price">{`Цена : ${price}`}</div>
      <div className="card__color">{color}</div>
      <div className="card__article">{`Артикль : ${article}`}</div>
      {option ? (
        <Button
          className="card__button"
          title={'Добавить в корзину'}
          onClick={addCart}
        />
      ) : (
        <Button
          id={id}
          title={'Удалить'}
          onClick={delet ? deletFromFavorite : deletFromCart}
        />
      )}
    </div>
  );
}
Watch.propTypes = {
  id: PropTypes.number,
  src: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  article: PropTypes.number,
  deletFromCart: PropTypes.func,
  option: PropTypes.bool,
  setStorage: PropTypes.func,
  storage: PropTypes.array,
  favorites: PropTypes.array,
  setFavorites: PropTypes.func,
  deletFromFavorite: PropTypes.func,
  delet: PropTypes.bool,
};

Watch.defaultProps = {
  id: null,
  src: null,
  price: null,
  color: null,
  article: null,
  option: false,
  storage: [],
  favorites: [],
  delet: false,
};
