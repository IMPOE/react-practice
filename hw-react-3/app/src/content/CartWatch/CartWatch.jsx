import { React, useState } from 'react';
import './CartWatch.scss';
import close from './icons8-close-window-48.png';
import PropTypes from 'prop-types';
export default function CartWatch({
  id,
  name,
  src,
  price,
  color,
  article,
  deletFromCart,
  sum,
  setSum,
}) {
  const [counter, setCounter] = useState(1);
  const [value, setValue] = useState(price);

  function decrementCounter() {
    setCounter(Math.max(counter - 1, 1));
    decrementValue();
  }

  function incrementValue() {
    setValue(value + price);
  }

  function decrementValue() {
    if (value === price) {
      return price;
    } else setValue(value - price);
  }

  function incrementCounter() {
    setCounter(Math.min(counter + 1, 100));
    incrementValue();
  }

  function changePrice(e) {}

  return (
    <div className="cart__block-container">
      <div className="cart__block">
        <img className="cart__block-img" src={src} alt="name"></img>
        <div className="cart__price">
          <span className="cart__price-iter" onClick={incrementCounter}>
            +
          </span>
          <input
            type="text"
            className="cart__input"
            placeholder="1"
            value={counter}
            onChange={changePrice}
          ></input>
          <span className="cart__price-iter" onClick={decrementCounter}>
            -
          </span>
        </div>
        <div className="cart__p">{value}</div>
        <img
          className="close"
          src={close}
          alt="close"
          onClick={deletFromCart}
        ></img>
      </div>
    </div>
  );
}
CartWatch.propTypes = {
  id: PropTypes.number,
  src: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  article: PropTypes.number,
  deletFromCart: PropTypes.func,
  sum: PropTypes.array,
  setSum: PropTypes.func,
  count: PropTypes.number,
  value: PropTypes.number,
};

CartWatch.defaultProps = {
  id: null,
  price: null,
  color: null,
  article: null,
  sum: [],
  count: null,
  value: null,
};
