import { useState, useEffect } from 'react';
export const useOnePost = () => {
  const url = '/api/watch';
  const [post, setPost] = useState([]);
  useEffect(() => {
    async function getPost() {
      const resp = await fetch(url);
      const fullData = await resp.json();
      setPost(fullData);
    }
    getPost();
  }, []);
  return [post, setPost];
};
