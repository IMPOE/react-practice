import React, {Component} from 'react';
import Button from './components/Button/Button.js';
import Modal from './components/Modal/Modal.js';
import './App.scss';
class App extends Component {
    state = {
        showModalOne: false,
        showModalTwo: false,
        buttons: {
            modalBtnOk: <button className="modal__btn modal__btnOk">OK</button>,
            modalBtnCancel: <button className="modal__btn modal__btnCancel">Cancel</button>
        },
    }


    toggleModalOne = () => {
        const {showModalTwo} = this.state
        this.setState({showModalOne: !this.state.showModalOne});
        this.changeBackground(showModalTwo)
    };

    toggleModalTwo = () => {
        const {showModalOne,} = this.state
        this.setState({showModalTwo: !this.state.showModalTwo});
        this.changeBackground(showModalOne)
    }

    clickOutside = (e) => {
        const outside = document.getElementById("App");
        if (e.target === outside) {
            this.setState({showModalOne: false})
            this.setState({showModalTwo: false})
            outside.style.backgroundColor = "white"
        }
    }

    changeBackground = (param) => {
        const outside = document.getElementById("App")
        outside.style.backgroundColor === "gray" ?
            outside.style.backgroundColor = "white" :
            outside.style.backgroundColor = "gray";
        if (param) {
            outside.style.backgroundColor = "gray"
        }
    }





    render(){
            const {buttons , showModalOne,showModalTwo} = this.state

        return (

                <div id="App" className="App" onClick={this.clickOutside}>
                    <div className="wrapper">

                        {/*HEADER BUTTONS */}
                        <div className="buttons">

                            <Button onClick={this.toggleModalOne}
                                    text={"Open first modal"}
                                    backgroundColor={"red"} className="btn"/>

                            <Button onClick={this.toggleModalTwo}
                                    text={"Open second modal"}
                                    backgroundColor={"#00ffff "}
                                    className="btn"/>
                        </div>

                            {/*MODAL SECTION */}
                                {showModalOne && <Modal
                                    header={"Do you want to delete first file?"}
                                       closeButton={false}
                                       text={"Once you delete this file, it won't be possible to undo this action. " +
                                       "Are you sure you want to delete it?"}
                                       action={buttons}
                                    />}

                                {showModalTwo && <Modal
                                    header={"Do you want to delete React JS?"}
                                       closeButton={true}
                                       text={" it won't be possible to undo this action" +
                                       "Are you sure you want to quit?"}
                                       action={buttons}
                                       backgroundColor={"blue"}
                                />}
                            </div>
                </div>
            );
    }
}

export default App;