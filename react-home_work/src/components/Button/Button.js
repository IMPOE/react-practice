import React, {Component} from 'react';
import "./Button.scss"
class Button extends Component {
    render() {
        const {text,backgroundColor,className ,onClick} = this.props
        return (
            <div>
                <button onClick={onClick}
                        style={{backgroundColor: backgroundColor}}
                        className={className} >
                    {text}</button>
            </div>
        );
    }
}

export default Button;