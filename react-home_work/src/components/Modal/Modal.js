import React, {Component} from 'react';
import "./Modal.scss"
class Modal extends Component {
    render() {
        const {header,closeButton,text,action,backgroundColor} = this.props
        const {modalBtnCancel,modalBtnOk} = action
        return (

            <div className="modal" style={{backgroundColor: backgroundColor}}>
                <div className="header__modal">
                    {header}
                    {closeButton && <button className="close__btn">X</button>}
                </div>

                <div className="body__modal">
                    {text}
                </div>

                <div className="footer__modal">
                    {modalBtnOk}
                    {modalBtnCancel}
                </div>

            </div>
        );
    }
}

export default Modal;