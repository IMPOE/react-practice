import React, { Component } from 'react';
import './Button.scss';
class Button extends Component {
  render() {
    const { text, backgroundColor, className, onClick, id } = this.props;
    return (
      <div>
        <button
          id={id}
          onClick={onClick}
          style={{ backgroundColor: backgroundColor }}
          className={className}
        >
          {text}
        </button>
      </div>
    );
  }
}

export default Button;
