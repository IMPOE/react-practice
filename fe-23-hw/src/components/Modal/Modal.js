import React, { Component } from 'react';
import Button from '../Button/Button';
import './Modal.scss';
class Modal extends Component {
  render() {
    const {
      header,
      closeButton,
      text,
      backgroundColor,
      className,
      id,
      toggleModalOne,
      toggleModalTwo,
      clickOutside,
    } = this.props;

    return (
      <div className="outside" onClick={clickOutside}>
        <div className="modal" style={{ backgroundColor: backgroundColor }}>
          <div className="header__modal">
            {header}
            {closeButton && (
              <Button
                onClick={toggleModalTwo && toggleModalOne}
                className={className}
                text="X"
                id={id}
              />
            )}
          </div>

          <div className="body__modal">{text}</div>

          <div className="footer__modal">
            <Button
              className="modal__btn modal__btnOk"
              text="OK"
              onClick={toggleModalTwo && toggleModalOne}
            />
            <Button
              className="modal__btn modal__btnCancel"
              text="Cancel"
              onClick={toggleModalTwo && toggleModalOne}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
