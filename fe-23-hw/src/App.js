import React, { Component } from 'react';
import Button from './components/Button/Button.js';
import Modal from './components/Modal/Modal.js';
import './App.scss';

const buttons = {
  modalBtnOk: <button className="modal__btn modal__btnOk">OK</button>,
  modalBtnCancel: (
    <button className="modal__btn modal__btnCancel">Cancel</button>
  ),
};

class App extends Component {
  state = {
    showModalOne: false,
    showModalTwo: false,
  };

  toggleModalOne = () => {
    this.setState({ showModalOne: !this.state.showModalOne });
  };

  toggleModalTwo = () => {
    this.setState({ showModalTwo: !this.state.showModalTwo });
  };

  clickOutside = (e) => {
    if (e.currentTarget.className === 'outside') {
      this.setState({ showModalTwo: false });
      this.setState({ showModalOne: false });
    }
  };

  render() {
    const { showModalOne, showModalTwo } = this.state;

    return (
      <div id="App" className="App">
        <div className="wrapper">
          <div className="buttons">
            <Button
              id={'One'}
              onClick={this.toggleModalOne}
              text={'Open first modal'}
              backgroundColor={'red'}
              className="btnOne"
              disabled={false}
            />
            <Button
              id={'Two'}
              onClick={this.toggleModalTwo}
              text={'Open second modal'}
              backgroundColor={'#00ffff '}
              className="btnTwo"
              disabled={false}
            />
          </div>
          {showModalOne && (
            <Modal
              clickOutside={this.clickOutside}
              id={'btnOne'}
              toggleModalOne={this.toggleModalOne}
              header={'Do you want to delete first file?'}
              closeButton={true}
              text={
                "Once you delete this file, it won't be possible to undo this action. " +
                'Are you sure you want to delete it?'
              }
              action={buttons}
              className={'btnOne'}
            />
          )}

          {showModalTwo && (
            <Modal
              clickOutside={this.clickOutside}
              id={'btnTwo'}
              toggleModalTwo={this.toggleModalTwo}
              header={'Do you want to delete React JS?'}
              closeButton={true}
              text={
                " it won't be possible to undo this action" +
                'Are you sure you want to quit?'
              }
              action={buttons}
              backgroundColor={'blue'}
              className={'btnTwo'}
            />
          )}
        </div>
      </div>
    );
  }
}

export default App;
